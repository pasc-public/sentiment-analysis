# ARCHIVE PYTHON SCRIPT FOR LAMBDA
data "archive_file" "lambda_file" {
  type        = "zip"
  source_file = "${path.module}/files/analyse.py"
  output_path = "${path.module}/lambda.zip"
}

# CREATE LAMBDA FUNCTION
resource "aws_lambda_function" "sentiment_analysis" {
  filename         = data.archive_file.lambda_file.output_path
  function_name    = "sentiment-analysis"
  role             = aws_iam_role.lambda_role.arn
  handler          = "analyse.lambda_handler"
  source_code_hash = filebase64sha256(data.archive_file.lambda_file.output_path)

  runtime = "python3.8"
}

resource "aws_cloudwatch_log_group" "sentiment_log" {
  name              = "/aws/lambda/sentiment-analysis"
  retention_in_days = 14
}