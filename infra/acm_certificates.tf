resource "aws_acm_certificate" "sentiment_labs_certificate" {
  provider = aws.ue1

  domain_name               = "sentiment.${var.domain_name}"
  validation_method         = "DNS"
  subject_alternative_names = ["*.sentiment.${var.domain_name}"]
}

# output "validation_options" {
#   value = element(aws_acm_certificate.sentiment_labs_certificate.domain_validation_options[*].resource_record_name, 1)
# }