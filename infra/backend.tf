# PROVIDER
provider "aws" {
  region = "eu-west-1"
}

provider "aws" {
  alias = "ue1"

  region = "us-east-1"
}

# BACKEND
terraform {
  backend "s3" {
    bucket = "terraform-projects-states-lab"
    key    = "talent-academy/sentiment-analysis/terraform.tfstates"
    region = "eu-west-1"
  }
}
