# resource "aws_cognito_user_pool" "pool" {
#   name = "cr_pool"
# }

# resource "aws_cognito_user_pool_client" "app_client" {
#   name                                 = "cr_appClient"
#   user_pool_id                         = aws_cognito_user_pool.pool.id
#   callback_urls                        = ["https://sentiment.${var.domain_name}/"]
#   logout_urls                          = ["https://auth.${var.domain_name}/logout"]
#   allowed_oauth_flows_user_pool_client = true
#   allowed_oauth_flows                  = ["code", "implicit"]
#   allowed_oauth_scopes                 = ["email", "openid"]
#   supported_identity_providers         = ["COGNITO"]
# }

# resource "aws_cognito_user_pool_domain" "main" {
#   count = aws_acm_certificate.sentiment_labs_certificate.status == "ISSUED" ? 1 : 0

#   domain          = "auth.sentiment.${var.domain_name}"
#   certificate_arn = aws_acm_certificate.sentiment_labs_certificate.arn
#   user_pool_id    = aws_cognito_user_pool.pool.id
# }