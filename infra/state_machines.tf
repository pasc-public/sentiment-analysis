resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "sentiment-analysis-machine"
  role_arn = aws_iam_role.states_role.arn

  definition = <<EOF
{
  "Comment": "Send text message to lambda for sentiment analysis",
  "StartAt": "SendMessage",
  "States": {
    "SendMessage": {
      "Type": "Task",
      "Resource": "${aws_lambda_function.sentiment_analysis.arn}",
      "End": true
    }
  }
}
EOF
}