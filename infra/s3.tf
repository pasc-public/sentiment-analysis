resource "aws_s3_bucket" "sentiment_website" {
  bucket = "sentiment.${var.domain_name}"
}

resource "aws_s3_bucket_acl" "sentiment_website_acl" {
  bucket = aws_s3_bucket.sentiment_website.id
  acl    = "public-read"
}

resource "aws_s3_bucket_policy" "sentiment_website" {
  bucket = aws_s3_bucket.sentiment_website.id
  policy = data.aws_iam_policy_document.s3_static_host.json
}

resource "aws_s3_bucket_website_configuration" "sentiment_website_config" {
  bucket = aws_s3_bucket.sentiment_website.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

resource "aws_cloudfront_origin_access_identity" "cdn_identity" {
  comment = "Identity for sentiment lab website"
}

resource "aws_cloudfront_distribution" "cdn" {
  count = aws_acm_certificate.sentiment_labs_certificate.status == "ISSUED" ? 1 : 0

  origin {
    domain_name = aws_s3_bucket.sentiment_website.bucket_regional_domain_name
    origin_id   = "myS3Origin"

    s3_origin_config {
      origin_access_identity = "origin-access-identity/cloudfront/${aws_cloudfront_origin_access_identity.cdn_identity.id}"
    }
  }

  enabled             = true
  default_root_object = "index.html"

  aliases = ["sentiment.${var.domain_name}"]

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "myS3Origin"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  # Cache behavior with precedence 0
  ordered_cache_behavior {
    path_pattern     = "/content/immutable/*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = "myS3Origin"

    forwarded_values {
      query_string = false
      headers      = ["Origin"]

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  # Cache behavior with precedence 1
  ordered_cache_behavior {
    path_pattern     = "/content/*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "myS3Origin"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  price_class = "PriceClass_200"

  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["GB"]
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.sentiment_labs_certificate.arn
    ssl_support_method  = "sni-only"
  }
}

output "identity_id" {
  value = aws_cloudfront_origin_access_identity.cdn_identity.id
}