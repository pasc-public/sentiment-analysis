import os, boto3
import json

client = boto3.client('comprehend')

def lambda_handler(event, context):
    responseMessage = "This is just a text"

    if "queryStringParameters" in event:
        raw_body = event['queryStringParameters']
        if "message_text" in raw_body:
            responseMessage = raw_body['message_text']

    sentiment=client.detect_sentiment(Text=responseMessage,LanguageCode='en')
    entities_raw=client.detect_entities(Text=responseMessage,LanguageCode='en')

    entities = {}

    for en in entities_raw["Entities"]:
        if en["Type"] in entities:
            entities[en["Type"]].append(en["Text"])
        else:
            entities[en["Type"]] = [en["Text"]]

    result = {}
    result["original_msg"] = responseMessage
    result["sentiment"] = sentiment["Sentiment"]
    # result["entities_raw"] = entities_raw
    result["entities"] = entities

    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-Type'] = 'application/json'
    # responseObject['body'] = json.dumps(event['queryStringParameters']['message_text'])
    responseObject['body'] = json.dumps(result)

    print(json.dumps(result))

    return responseObject